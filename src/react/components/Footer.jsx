import React from 'react'
import PropTypes from 'prop-types'

const Footer = ({year}) => {
	return (
		<div class="inner">
			<ul class="menu">
				<li>&copy; AlisonGaleon[dot]com {year}. All rights reserved.</li><li>Theme Based: <a href="https://html5up.net/hyperspace">Hyperspace by HTML5 UP @ajlkn</a></li>
			</ul>
		</div>
	)
}

Footer.propTypes = {
	year: PropTypes.number,
}

Footer.defaultProps = {
	year: new Date().getFullYear(),
}

export default Footer