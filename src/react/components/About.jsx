import React from 'react'

const About = () => {
	return (
		<div class="inner">
			<h1><small>/</small> About </h1>
			<p>
				Hi!, I am Alison Galeon and I am a <a title="Front- and Back-end Development">Full Stack Web Deloper</a> for over 4 years now.<br />
				I grow up in the Philippines, graduated at Liceo de Cagayan University with Bachelor's Degree in Information Technology. I am currently based in Brunei with my wife.<br />
				I enjoy learning to code more, cooking and the sight of nature.<br /><br />
				I am on <a href="https://github.com/xiomai">github</a> and <a href="https://www.facebook.com/xiomailuv">facebook</a>.<br />
				Fan of <a href="http://calvinandhobbes.alisongaleon.com/">Calvin and Hobbes</a> and LeBron James.
				</p>
			<ul class="actions">
				<li><a href="#one" class="button scrolly">Learn more</a></li>
			</ul>
		</div>
	)
}

export default About