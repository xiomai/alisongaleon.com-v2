import React from 'react'

const Sidebar = () => {
	return (
		<div class="inner">
			<nav>
				<ul>
					<li><a href="#intro">About</a></li>
					<li><a href="#one">Language and Tools</a></li>
					{/*<li><a href="#two">Projects</a></li>*/}
					<li><a href="#three">Contact</a></li>
				</ul>
			</nav>
		</div>
	)
}

export default Sidebar