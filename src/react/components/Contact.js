import axios from 'axios'
import $ from 'jquery'
import React from 'react'
import ReCAPTCHA from 'react-google-recaptcha'

import SendMessageBtn from './_sub/SendMessageBtn'
import SmallHTML from './_sub/SmallHTML'

export default class Contact extends React.Component {

	constructor() {
		super()
		this.handleValidate = this.handleValidate.bind(this)
		this.handleSubmitMail = this.handleSubmitMail.bind(this)
		this.handlePostEmail = this.handlePostEmail.bind(this)
		this.handleEmailPostResponse = this.handleEmailPostResponse.bind(this)
		this.clearFields = this.clearFields.bind(this)
		this.handleRecaptcha = this.handleRecaptcha.bind(this)
		this.recaptchaSiteKey = '6LfzBCMUAAAAAKJn60L9y8mvK_edLTXKNBob5jHw'
		this.state = {
			isNameValid: true,
			isEmailValid: true,
			isMessageValid: true,
			nameFlag: '',
			emailFlag: '',
			messageFlag: '',
			submitError: false,
			sending: false,
			sent: false,
			sendFail: false,
			recaptchaOK: false
		}
		this.nameMin = {value: 4, info: 'Name should be atleast 4 characters minimum.'},
		this.messageMin = {value: 10, info: 'Name should be atleast 10 characters minimum.'}
		this.emailErrInfo = 'Please use a valid email address.'
	}

	clearFields() {
		const name = $('#name').val('')
		const email = $('#email').val('')
		const message = $('#message').val('')
	}

	validateEmail(email) {
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

		let testMail = re.test(email)
		testMail ? this.setState({isEmailValid: true, emailFlag: ''}) : this.setState({isEmailValid: false, emailFlag: 'error'})
		if (email.length==0) this.setState({emailFlag: ''})
		return testMail;
	}

	handleSubmitMail() {
		this.setState({sent: false, submitError: false, sendFail: false})
		const name = $('#name').val()
		const email = $('#email').val()
		const message = $('#message').val()

		const {nameFlag, emailFlag, messageFlag, recaptchaOK} = this.state

		if (nameFlag == '' && name.length != 0 && 
			 emailFlag == '' && email.length != 0 &&
			 messageFlag == '' && message.length != 0 &&
			 recaptchaOK) {

			this.handlePostEmail([name, email, message])
		} else {
			console.log('here');
			this.setState({submitError: true})
		}

	}

	handlePostEmail(pdata) {
		
		const [name, email, message] = pdata

		this.setState({sending: true})

		axios.post('includes/mailer.php', {
			name: name,
			email: email,
			message: message
		})
		.then((response) => {
			this.handleEmailPostResponse(response)
		})
		.catch((error) => {
			this.setState({sending: false, sent: false, sendFail: true})
			console.log('***Catch***: ', error)
		})
	}

	handleEmailPostResponse(response) {
		const {isSent} = response.data
		isSent ? (
			this.setState({sending: false, sent: true, submitError: false, recaptchaOK: false}),
			this.clearFields()
		) : (
			this.setState({sending: false, sent: false, sendFail: true})
		)
	}

	handleValidate(e) {
		this.setState({sent: false, submitError: false})
		const {id, value, length} = e.target
		const trimmedValue = value.trim()
		const valueLen = trimmedValue.length
		
		switch(id) {
			case 'email':
				this.validateEmail(value)
				break
			case 'name':
				if (valueLen < this.nameMin.value) {
					this.setState({isNameValid: false, nameFlag: 'error'})
					if (valueLen == 0) this.setState({nameFlag: ''})
					break
				}
				this.setState({isNameValid: true, nameFlag: ''})
				break
			case 'message':
				if (valueLen < this.messageMin.value) {
					this.setState({isMessageValid: false, messageFlag: 'error'})
					if (valueLen == 0) this.setState({messageFlag: ''})
					break
				}
				this.setState({isMessageValid: true, messageFlag: ''})
				break
		}
	}

	handleRecaptcha(value) {
		this.setState({sent: false, submitError: false})
		const recaptchaResponse = value
		axios.post('includes/recaptcha.php', {
			response: recaptchaResponse
		})
		.then((response) => {
			const {success} = response.data
			this.setState({recaptchaOK: success})
		})
		.catch((error) => {
			this.setState({sending: false, sent: false, sendFail: true, recaptchaOK: false})
			console.log('***Catch***: ', error)
		})
	}

	render() {

		const {nameFlag, emailFlag, messageFlag, submitError, sending, sent, sendFail, recaptchaOK} = this.state

		const nameErrInfo = nameFlag !== '' ? <SmallHTML flag='warning' textInfo={this.nameMin.info} />  : ''
		const emailErrInfo = emailFlag !== '' ? <SmallHTML flag='warning' textInfo={this.emailErrInfo} /> : ''
		const messageErrInfo = messageFlag !== '' ? <SmallHTML flag='warning' textInfo={this.messageMin.info} /> : ''

		return (
			<div class="inner">
				<h2><small>/</small> Get in touch</h2>
				<div class="split style1">
					<section>
							<div class="field half first">
								<label for="name">Name {nameErrInfo}</label>
								<input onBlur={this.handleValidate.bind(event)} type="text" class={nameFlag} name="name" id="name" />
							</div>
							<div class="field half">
								<label for="email">Email {emailErrInfo}</label>
								<input onBlur={this.handleValidate.bind(event)} type="email" class={emailFlag} name="email" id="email" />
							</div>
							<div class="field">
								<label for="message">Message {messageErrInfo}</label>
								<textarea onBlur={this.handleValidate.bind(event)} name="message" class={messageFlag} id="message" rows="5"></textarea>
							</div>
							<ul class="actions">
								<li>
								<ReCAPTCHA ref="recaptcha" theme="dark" sitekey={this.recaptchaSiteKey} onChange={this.handleRecaptcha} />
								<br /><SendMessageBtn sendFail={sendFail} sending={sending} sent={sent} submitError={submitError} recaptchaOK={recaptchaOK} onClick={this.handleSubmitMail.bind(this)} /></li>
							</ul>
					</section>
					<section>
						<ul class="contact">
							<li>
								<h3>Email</h3>
								<a href="mailto:me@alisongaleon.com">me@alisongaleon.com</a>
							</li>
							<li>
								<h3>Phone</h3>
								<span>(673) 883-6224</span>
							</li>
							<li>
								<h3>Social</h3>
								<ul class="icons">
									<li><a href="https://github.com/xiomai" class="fa-github"><span class="label">GitHub</span></a></li>
									<li><a href="https://www.linkedin.com/in/alison-galeon-a54b7549/" class="fa-linkedin"><span class="label">LinkedIn</span></a></li>
									<li><a href="https://www.facebook.com/xiomailuv" class="fa-facebook"><span class="label">Facebook</span></a></li>
									<li><a href="https://twitter.com/xiomaidotnet" class="fa-twitter"><span class="label">Twitter</span></a></li>
									<li><a href="https://www.instagram.com/xiomaidotnet/" class="fa-instagram"><span class="label">Instagram</span></a></li>
								</ul>
							</li>
						</ul>
					</section>
				</div>
			</div>
			);
	}

}