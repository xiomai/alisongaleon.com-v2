import React, {PureComponent} from 'react'

export default class SendingBtn extends PureComponent {
	render() {
		return (
			<div>
				<a class="button submit disabled">Sending...</a>
			</div>
		)
	}
}