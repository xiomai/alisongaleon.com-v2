import React, {PureComponent} from 'react'

export default class SmallHTML extends PureComponent {

	render() {
		const classtag = `info ${this.props.flag}`
		return (
			<small class={classtag}> {this.props.textInfo} </small>
		)
	}
} 