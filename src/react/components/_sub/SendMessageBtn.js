import React, {PureComponent} from 'react'

import SendingBtn from './SendingBtn'
import SmallHTML from './SmallHTML'

export default class SendMessageBtn extends PureComponent {
	render() {
		const {sending, sent, sendFail, submitError, onClick, recaptchaOK} = this.props

		const submitErrInfo = submitError ? <SmallHTML flag='warning' textInfo='Please fill the form properly.' /> : ''
		
		const button = sending ? <SendingBtn /> : (
			<a onClick={onClick} class="button submit">Send Message</a>
		)

		const sentInfo = sent ? (
			<SmallHTML flag='success' textInfo='Message sent. Thank you!' />
		) : ''

		const sentFailInfo = sendFail ? (
			<SmallHTML flag='danger' textInfo='Message not sent. Please directly send via email below.' />
		) : ''

		const recaptchaInfo = (submitError && !recaptchaOK) ? (
			<SmallHTML flag='danger' textInfo='Please verify recaptcha.' />
		) : ''

		return (
			<div>{button} {submitErrInfo} {sentInfo} {sentFailInfo} {recaptchaInfo}</div>
		)
	}
}