import React from 'react';
import ReactDOM from 'react-dom'

import About from './components/About'
import Contact from './components/Contact'
import Footer from './components/Footer'
import LanguageAndTools from './components/LanguageAndTools'
import Sidebar from './components/Sidebar'

const footer = document.querySelector('#footer')
const intro = document.querySelector('#intro')
const one = document.querySelector('#one')				// About
const sidebar = document.querySelector('#sidebar')
const three = document.querySelector('#three')			// Contact

ReactDOM.render(<About />, intro)
ReactDOM.render(<Contact />, three)
ReactDOM.render(<Footer />, footer)
ReactDOM.render(<LanguageAndTools />, one)
ReactDOM.render(<Sidebar />, sidebar)

if (DEVELOPMENT) {
    if (module.hot) {
        module.hot.accept();
    }
}