var templateAssets = './template/html5up-hyperspace/assets/'
/* Template Sass */
var mainSass = require(templateAssets+'sass/main.scss')
/* Template JS */
var jqueryScrollex = require(templateAssets+'js/jquery.scrollex.min.js')
var jqueryScrolley = require(templateAssets+'js/jquery.scrolly.min.js')
var jsutil = require(templateAssets+'js/util.js')
var templateMain = require(templateAssets+'js/main.js')

var react = require('./react/index.js')