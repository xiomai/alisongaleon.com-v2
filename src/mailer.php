<?php
require '../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
$_POST = json_decode(file_get_contents('php://input'), true); //using axios

$method = $_SERVER['REQUEST_METHOD'];
if ($method == 'POST') {
	$from = $_POST['email'];
	$name = $_POST['name'];
	$message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);

	$body = '';
	$body .= '<h1>AlisonGaleon.com Cotact Form Message</h1>';
	$body .= '<h2>From: <strong>'.$name.'</strong> - <em>'.$from.'</em></h2>';
	$body .= '<hr />';
	$body .= $message;

	
	sendMail($from, $name, $body, false);
}
function sendMail($from, $name, $body, $debug = true) {

	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();
	if ($debug) {
		$mail->SMTPDebug = 2;
		$mail->Debugoutput = 'html';

	} else {
		$mail->SMTPDebug = 0;
	}
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 587;
	$mail->SMTPSecure = 'tls';
	$mail->SMTPAuth = true;
	$mail->Username = "noreply@alisongaleon.com";
	$mail->Password = "noreplymail";
	$mail->setFrom($from, $name);
	$mail->addReplyTo($from, $name);
	$mail->addAddress('me@alisongaleon.com', 'Alison Galeon');
	$mail->Subject = 'AlisonGaleon[dot]com Contact Form Message';
	$mail->isHTML(true);
	$mail->Body = $body;
	if (!$mail->send()) {
		echo json_encode(array('isSent' => false));
	} else {
		echo json_encode(array('isSent' => true));
	}
}