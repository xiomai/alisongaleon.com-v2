var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HTMLWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

const DEVELOPMENT = process.env.NODE_ENV === 'development';
const PRODUCTION = process.env.NODE_ENV === 'production';

const entry = PRODUCTION
	?	[
			'./src/index.js'
		]
	:	[
			'./src/index.js',
			'webpack/hot/dev-server',
			'webpack-dev-server/client?http://localhost:8080'
		];

const plugins = PRODUCTION
	? 	[
			new webpack.DefinePlugin({
				'process.env': {
					NODE_ENV: JSON.stringify('production')
				}
			}),
			new webpack.optimize.UglifyJsPlugin({
          output: {
            comments: false,
          },
        }),
			new ExtractTextPlugin('style-[contenthash:10].css'),
			new HTMLWebpackPlugin({
				template: './src/template/html5up-hyperspace/index.html',
				favicon: './src/images/favicon.png'
			}),
			new CopyWebpackPlugin([{ from: './src/mailer.php', to: 'includes/'}]),
			new CopyWebpackPlugin([{ from: './src/recaptcha.php', to: 'includes/'}])
		]
	: 	[
			new webpack.HotModuleReplacementPlugin(),
			new webpack.NamedModulesPlugin(),
			new HTMLWebpackPlugin({
				template: './src/template/html5up-hyperspace/index.html',
				favicon: './src/images/favicon.png'
			}),
			new webpack.ProvidePlugin({
				$: "jquery",
				jQuery: "jquery"
			}),
			function() {
				this.plugin('watch-run', function(watching, callback) {
					console.log('Begin compile at ' + new Date());
					callback();
				})
			}
		];

plugins.push(
	new webpack.DefinePlugin({
		DEVELOPMENT: JSON.stringify(DEVELOPMENT),
		PRODUCTION: JSON.stringify(PRODUCTION)
	})
);

const cssIdentifier = PRODUCTION ? '[hash:base64:10]' : '[path][name]__[local]--[hash:base64:5]';

const cssLoader = PRODUCTION
	?	ExtractTextPlugin.extract({
			use: ['css-loader?minimize&localIdentName=' + cssIdentifier, 'sass-loader']
		})
	: 	['style-loader', 'css-loader?sourceMap&localIdentName=' + cssIdentifier, 'sass-loader?sourceMap'];

module.exports = {
	devtool: PRODUCTION ? 'nosources-source-map': 'cheap-module-eval-source-map',
	entry: entry,
	plugins: plugins,
	externals: {
		jquery: 'jQuery', //jquery is external and available at the global variable jQuery
	},
	module: {
		rules: [{
			test: /.jsx?$/,
			use: ['babel-loader'],
			exclude: /node_modules/
		}, {
			test: /\.(png|jpg|gif)$/,
			use: ['url-loader?limit=10000&name=images/[hash:12].[ext]'],
			exclude: /node_modules/
		}, {
			test: /\.(css|scss)$/,
			use: cssLoader,
			exclude: /node_modules/
		}, { 
			test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
			use: "url-loader?limit=10000&mimetype=application/font-woff" 
		}, { 
			test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, 
			use: "file-loader" 
		}, { 
			test: /\.html$/, 
			use: 'html-loader',
			exclude: /node_modules/ 
		}]
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	output: {
		path: path.join(__dirname, 'public'),
		// publicPath: '/v2/',
		filename: 'bundle.[hash:12].min.js'
	}
};
